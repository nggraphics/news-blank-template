# National Geographic Basic Template

Basic blank template for National Geographic interactive graphics. Supports SASS. Shows changes without refreshing the window manually. Includes basic CSS and JS  from the National geographic website.

Please note: do not reproduce National Geographic logos or fonts without written permission.

## Prerequisites

- Install [Node.js](http://nodejs.org/).
- Install [hub](https://github.com/github/hub) (on OS X, using [Homebrew](http://brew.sh)) and wget: `brew install hub wget git-extras`.
- Install various CSS utilities: `gem install sass compass breakpoint`.

## Creating a new project
- Download a zip of this repository
- Name it
- Access it through the Terminal
- ONLY THE FIRST TIME: Run `npm install -g`. If this gives you any errors run it again as sudo, e.g. `sudo npm install -g`.
- Type `gulp`
- When prompted to compile or not, say NO (You can just hit enter)
- It will open a new tab in your browser with a placeholder article and the graphic midway in it.

## Opening the project
- If you are using [Sublime Text](http://www.sublimetext.com/) (recommended) as your HTML editor, go to Project/OPen Project and select `graphic.sublime.project`. It will open just the files you might need to use.
- If you use a different HTML editor, just open the folder you have created.


## Basic files to use

- The three basic files you have to worry about:
	- `html/html.html`: The HTML, of course. Includes Headline, Dek, sources... Also, comments on how to change the size of the project to Cinematic, Medium... By default, it is column-wide
	- `scss/_styles.scss`: Basic CSS. Includes two breakpoints for responsive design. It is using SCSS, what means you can nest styles (nest everything under #gf to be sure you are only pointing to styles in your graphic and not other elements in the page)
	- `js/code.js`: Javascript code. Get into the default function what you want to run by default. Functions outside it can be called when needed.

- Whatever you change in the project will be automatically republished in that tab on your browser each time you save a file.

## Sharing, editing

- The browser window where you see your changes can be shared inside the network and it will be updated live as you change it. This is very useful to show the graphic to editors, writers. But also to see changes in phones, iPad, etc while working.
- Your default URL is `localhost:3000` but localhost can be switched to your IP.
- To get your IP, search for `Network Utility` in your spotlight. It will be there under IP address. Use it instead of localhost (f.i. `192.168.193.171:3000`)
- That is the URL you can share

## Compiling

- Once you're graphic is ready, get bakc to the Terminal
- Click `Ctrl + C` to stop the server. Once you do this, your graphic won't be visible at the URL we had.
- Write `gulp` again and hit `Enter`.
- When prompted to compile or not, say YES (`y`+`Enter`)
- In the folder, open the file called `COMPILED.html`. That file contains everything that needs to be uploaded, in one HTML file. That's the only thing you need to upload to AEM.


# Advanced options

## Including data, libraries

- Inside the HTML folder you will see `html/css.html` and `html/js.html`.
- You can add libraries, data and other files to your scss and js folders and call them here. They will be added to the compiled file.

## SCSS files

- In the scss folders, there are different files: `scss/_basics.scss`, `scss/_type.scss`, `scss/_color.scss`... These are basics css files that are added by default. You can add your own (using always the _ before the name and the scss extension) and call them in main.scss.
- In some of those files, we have variables with pre-established colors and font-families we can reuse. For instance, we set `$yellow` as `#FC0`. So everytime you need to use the yellow in your css, you can just use `background:$yellow`. It will help us to create consistent color palettes and font styles.

## Map fonts

- If we want to use map fonts in a graphic, we need to include them. The process to do it is explain in the [NatGeo Github](https://github.com/natgeo/font-maps/).

## Using templates

- We are building a [collection of basic graphic templates](https://github.com/natgeo-graphics/NatGeo_Templates) (still work in progress).
- If you want to use one, once you have downloaded the Blank Template, download the Templates set. Open the folder of the desired template and move the folders inside it to your Blank Template folder, subtituting the existing ones. 
- When you'll open the project, you will see an existing graphic. The data is included in the JS folder (will include Excel examples) and `code.js` include some variables for customization.
- To use new data, the easiest way is using the same structure in Excel and convert it to JSON using [Mr. Data Converter](http://shancarter.github.io/mr-data-converter/).

# Still to come

## Cleaning project
## Finishing templates
## Creating Yeoman generator
## Including project in general NatGeo GitHub





