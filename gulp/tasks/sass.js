var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var gulpif         = require('gulp-if');
var csso           = require('gulp-csso');

gulp.task('sass', function () {
    gulp.src('main.scss', {cwd: 'css'})
        .pipe(sass())
        .pipe(gulpif('*.css', csso(true)))
        .pipe(gulp.dest('.tmp'))
        .pipe(browserSync.reload({stream:true}));
});